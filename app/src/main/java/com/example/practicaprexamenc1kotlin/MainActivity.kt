package com.example.practicaprexamenc1kotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    //Variables de entradas
    private lateinit var txtNom: EditText

    //Variables de tipo boton
    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()

        btnEntrar.setOnClickListener { Ingresar() }

        btnSalir.setOnClickListener { Salir() }
    }

    //Funcion para hacer la relacion de los elementos
    private fun iniciarComponentes(){
        txtNom = findViewById(R.id.txtNom)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    //Funcion para entrar al sistema
    private fun Ingresar(){
        //Validacion de ingreso de campos

        //Validacion de ingreso de campos
        if (txtNom.text.toString() == "") {
            Toast.makeText(
                applicationContext,
                "Ingrese un nombre", Toast.LENGTH_LONG
            ).show()
        } else {
            val bundle = Bundle()
            bundle.putString("nombre", txtNom.text.toString())
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            txtNom.setText("")
        }
    }

    //Funcion para salir de la aplicacion
    private fun Salir(){
        finish()
    }
}