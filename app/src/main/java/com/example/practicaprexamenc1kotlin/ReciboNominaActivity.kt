package com.example.practicaprexamenc1kotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.lang.String

class ReciboNominaActivity : AppCompatActivity() {
    //Variables de entrada
    private lateinit var lblNumRecibo: TextView
    private lateinit var lblNombre: TextView
    private lateinit var lblNombreMain: TextView
    private lateinit var txtHorasNormal: EditText
    private lateinit var txtHorasExtras: EditText
    private lateinit var rdbAuxiliar: RadioButton
    private lateinit var rdbAlbanil: RadioButton
    private lateinit var rdbIngObra: RadioButton
    private lateinit var radioGrupo: RadioGroup

    //Variables de salidas
    private lateinit var lblImpuestoPor: TextView
    private lateinit var lblSubtotal: TextView
    private lateinit var lblimpuestp: TextView
    private lateinit var lblTotal: TextView

    //Variables de tipo de boton
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    //Creacion del objeto Recibo
    private var recibo = ReciboNomina(0,"",0.0F,
        0.0F,0,0.0F)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)
        iniciarComponentes()
        val bundle = intent.extras
        lblNombre.text = bundle!!.getString("nombre")
        lblNombreMain.text = bundle.getString("nombre")

        btnCalcular.setOnClickListener { Calcular() }

        btnLimpiar.setOnClickListener { Limpiar() }

        btnRegresar.setOnClickListener { Regresar() }
    }

    //Funcion donde se relacionana los elementos
    fun iniciarComponentes(){
        lblNumRecibo = findViewById(R.id.lblNumRecibo)
        lblNombre = findViewById(R.id.lblNombre)
        lblNombreMain = findViewById(R.id.lblNombreMain)
        txtHorasNormal = findViewById(R.id.txtHorasNormal)
        txtHorasExtras = findViewById(R.id.txtHorasExtras)
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar)
        rdbAlbanil = findViewById(R.id.rdbAlbanil)
        rdbIngObra = findViewById(R.id.rdbIngObra)
        radioGrupo = findViewById(R.id.grupoBotones)

        lblImpuestoPor = findViewById(R.id.lblImpuestoPor)
        lblSubtotal = findViewById(R.id.lblSubtotal)
        lblimpuestp = findViewById(R.id.lblImpuestoPor)
        lblTotal = findViewById(R.id.lblTotal)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
    }

    //Funcion para llamar al proceso de calcular
    private fun calculoRep(){
        recibo.horasTrabNormal = txtHorasNormal.text.toString().toFloat()
        recibo.horasTrabExtras = txtHorasExtras.text.toString().toFloat()
        lblSubtotal.text = String.valueOf(recibo.calcularSubtotal())
        lblImpuestoPor.text =
            String.valueOf(recibo.calcularImpuesto(lblSubtotal.text.toString().toFloat()))
        lblTotal.text =
            String.valueOf(
                recibo.calcularTotal(
                    lblImpuestoPor.text.toString().toFloat(),
                    lblSubtotal.text.toString().toFloat()
                )
            )
    }

    //Funcion de los botones
    private fun Calcular(){
        val validacion = !rdbAuxiliar.isChecked && !rdbAlbanil.isChecked && !rdbIngObra.isChecked

        if (lblNumRecibo.text.toString().trim { it <= ' ' }.isEmpty() ||
            lblNombre.text.toString().trim { it <= ' ' }.isEmpty() ||
            txtHorasNormal.text.toString().trim { it <= ' ' }.isEmpty() ||
            txtHorasExtras.text.toString().trim { it <= ' ' }.isEmpty() || validacion){
            Toast.makeText(
                applicationContext,
                "Ingrese Datos o Elija Datos", Toast.LENGTH_LONG
            ).show()
        } else {
            if (rdbAuxiliar.isChecked) {
                recibo.puesto = 1
                calculoRep()
            } else if (rdbAlbanil.isChecked) {
                recibo.puesto = 2
                calculoRep()
            } else if (rdbIngObra.isChecked) {
                recibo.puesto = 3
                calculoRep()
            }
        }
    }

    private fun Limpiar(){
        lblNumRecibo.text = ""
        txtHorasNormal.setText("")
        txtHorasExtras.setText("")
        lblImpuestoPor.text = ""
        lblSubtotal.text = ""
        lblTotal.text = ""
    }

    private fun Regresar(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Nomina")
        confirmar.setMessage(" ¿Desea regresar? ")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }

        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which -> dialog.dismiss() }

        confirmar.show()
    }
}