package com.example.practicaprexamenc1kotlin

class ReciboNomina {
    //Declaracion de variables necesarias
    var numRecibo: Int = 0
    var nombre: String = ""
    var horasTrabNormal: Float = 0.0F
    var horasTrabExtras: Float = 0.0F
    var puesto: Int = 0
    var impuestoPorc: Float = 0.0F

    //Creacion del constructor
    constructor(numRecibo:Int, nombre:String, horasTrabNormal:Float, horasTrabExtras:Float,
                puesto:Int, impuestoPorc:Float){
        this.numRecibo = numRecibo
        this.nombre = nombre
        this.horasTrabExtras = horasTrabExtras
        this.horasTrabNormal = horasTrabNormal
        this.puesto = puesto
        this.impuestoPorc = impuestoPorc
    }

    //Funciones necesarias
     fun Calcular(pago: Float): Float {
        var calculo: Float
        calculo = pago * horasTrabNormal
        if (horasTrabExtras > 0) {
            calculo = calculo + horasTrabExtras * pago * 2
        }
        return calculo
    }

    //Funcion para calcular el SubTotal
    fun calcularSubtotal(): Float {
        var subTotal = 0.0f
        return when (puesto) {
            1 -> {
                subTotal = Calcular(240f)
                subTotal
            }

            2 -> {
                subTotal = Calcular(300f)
                subTotal
            }

            3 -> {
                subTotal = Calcular(400f)
                subTotal
            }

            else -> subTotal
        }
    }

    //Funcion para calcular el Impuesto
    fun calcularImpuesto(subTotal: Float): Float {
        impuestoPorc = subTotal * 0.16f
        return impuestoPorc
    }

    //Funcion para calcular el Total a pagar al empleado
    fun calcularTotal(impuesto: Float, subTotal: Float): Float {
        return subTotal - impuesto
    }
}